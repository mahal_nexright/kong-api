from fastapi import FastAPI, Form
import uvicorn
import json
import os
from dotenv import load_dotenv
from service.service import authorization, get_refresh_token

app = FastAPI()
load_dotenv()

PROVISION_KEY = os.getenv("PROVISION_KEY")

KONG_ADMIN = os.getenv("KONG_ADMIN")

KONG_API = os.getenv("KONG_API")

API_PATH = os.getenv("API_PATH")

SERVICE_HOST = os.getenv("SERVICE_HOST")

SCOPES = json.loads(os.getenv("SCOPES"))

# authorize api for authentication of user and generate token
@app.post("/authorize/")
def authorize(
    client_id: str = Form(...),
    tenant_id: str = Form(...),
    response_type: str = Form(...),
    email: str = Form(...),
    password: str = Form(...),
):
    data = authorization(client_id, tenant_id, response_type, email, password)
    return data


# refresh token api incase of token expires after a certain period of time
@app.post("/authorize/refreshtoken/")
def generate_refresh_token(grant_type: str = Form(...), refresh_token: str = Form(...)):
    data = get_refresh_token(grant_type, refresh_token)
    return data


if __name__ == "__main__":
    uvicorn.run("app:app", port=5000, host="0.0.0.0", reload=True)
