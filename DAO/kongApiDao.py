from utils.connectionManager import initiateConnectionToMongoDB


client = initiateConnectionToMongoDB()
db = client["cognitiveview"]
cognitiveview_Collection = db["Client_Configurations_Collection"]
