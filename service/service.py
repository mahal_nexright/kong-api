from fastapi import HTTPException
import requests
from requests.auth import HTTPBasicAuth
import os
import json
from dotenv import load_dotenv
from urllib.parse import urlsplit, parse_qs


load_dotenv()

PROVISION_KEY = os.getenv("PROVISION_KEY")

KONG_ADMIN = os.getenv("KONG_ADMIN")

KONG_API = os.getenv("KONG_API")

API_PATH = os.getenv("API_PATH")

SERVICE_HOST = os.getenv("SERVICE_HOST")

CLIENT_ID = os.getenv("CLIENT_ID")

CLIENT_SECRET = os.getenv("CLIENT_SECRET")

SCOPES = json.loads(os.getenv("SCOPES"))

REDIRECT_URI = os.getenv("REDIRECT_URI")


# function to authorize the users and provide access token
def authorization(client_id, tenant_id, response_type, email, password):
    user_exists = check_user_exists(client_id, tenant_id, email, password)
    print(user_exists)
    if user_exists:
        response_data = requests.post(
            "https://127.0.0.1:8443/afcaInsights/oauth2/authorize",
            headers={"HOST": SERVICE_HOST},
            data={
                "client_id": CLIENT_ID,
                "response_type": response_type,
                "provision_key": PROVISION_KEY,
                "authenticated_userid": client_id + "_" + tenant_id,
            },
            verify=False,
        )
        print(response_data.text)
        if response_data.status_code == 200:
            print(REDIRECT_URI)
            response_data = response_data.json()
            redirect_url = response_data.get("redirect_uri")
            print(redirect_url)
            params = dict(parse_qs(urlsplit(redirect_url).query))
            response_data = requests.post(
                "https://127.0.0.1:8443/afcaInsights/oauth2/token",
                headers={"Host": SERVICE_HOST},
                data={
                    "grant_type": "authorization_code",
                    "client_id": CLIENT_ID,
                    "client_secret": CLIENT_SECRET,
                    "redirect_uri": REDIRECT_URI,
                    "code": params.get("code"),
                },
                verify=False,
            )
            print(response_data.text)
            if response_data.status_code == 200:
                print(response_data.json())
                if response_data:
                    return response_data.json()
            else:
                raise HTTPException(
                    status_code=404, detail="Error while generating token"
                )

    else:
        raise HTTPException(
            status_code=404, detail="Please provide valid client_id or tenant_id"
        )


# function to refresh the existing accesstoken
def get_refresh_token(grant_type, refresh_token):
    response_data = requests.post(
        "https://127.0.0.1:8443/afcaInsights/oauth2/token",
        headers={"Host": SERVICE_HOST},
        data={
            "grant_type": grant_type,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            "refresh_token": refresh_token,
        },
        verify=False,
    )
    if response_data.status_code == 200:
        if response_data:
            return response_data.json()
    else:
        raise HTTPException(
            status_code=404, detail="error while generating refresh token"
        )


# function to validate the user exists in cognitiveview
def check_user_exists(client_id, tentant_id, email, password):

    url = "https://app.cognitiveview.com:3000/cognitiveViewRestService/loginuser"
    payload = {
        "grant_type ": "client_credentials",
        "email": email,
        "password": password,
        "clientId": client_id,
        "tenantId": tentant_id,
    }
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    Username = email + "_" + client_id
    Password = password
    auth = HTTPBasicAuth(Username, Password)

    response = requests.post(url, headers=headers, auth=auth, data=payload)
    response = response.json()
    if response["status"] == 200:
        return True

    elif response["status"] == 500:
        raise HTTPException(
            status_code=500, detail="Some error occurred at server please contact admin"
        )

    else:
        raise HTTPException(status_code=404, detail="Please Provide valid Credential")
