# Basic Commands to build and start the container

The following commands will be used to build the docker image from the dockerfile and after that a container will be run using the image.

1. Build the image from dockerfile.

   - docker build -t kong-api .

2. Run the container using the built image by using the image name.

   - docker run -d --name kong-api -p 5000:5000 kong-api

### note the name kong-api is user specific you can use any name as per your choice as the name of the image.

## Some basic commands for using container.

- **Stoping the container**
  - docker stop kong-api
- **Starting the container**
  - docker start kong-api
- **Check if containers are running**
  - docker ps
- **check all the containers and as well as the stop containers**
  - docker ps -a
- **Checking for logs of container**
  - docker logs kong-api
