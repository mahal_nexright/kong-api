# Description - Connection file to initiate connection to MongoDB

import pymongo
import os
import json

from dotenv import load_dotenv

load_dotenv()


def initiateConnectionToMongoDB():
    MONGODB_URL = os.getenv("MONGODB_URL")
    os.path.join("certs", "mongo.crt")
    return pymongo.MongoClient(
        MONGODB_URL,
        replicaset="replset",
        ssl_ca_certs=os.path.join("certs", "mongo.crt"),
    )
