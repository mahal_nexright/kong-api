from typing import Optional
from pydantic import BaseModel, Field

class Auth_data(BaseModel):
    authenticated_userid : str
    response_type : str